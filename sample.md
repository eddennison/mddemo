Markdown
=
[Markdown](https://daringfireball.net/projects/markdown/)  is a minimalist markup language (with a Perl reference implementation) that converts human-readable text into HTML. Markdown support is built into BitBucket and GitHub. Any file with the extension **.md** is treated as a Markdown file and automatically rendered as HTML.

# Paragraphs

Paragraphs begin with blank lines. 
No blank line, no new paragraph.

Blank line, new paragraph.

# Headings Start with a # Character
## Sub-Headings with Multiple # Characters

All headings generate an HTML anchor of the form **markdown-header-header-text** where ***header-text*** is the header text in all lower case with whitespace replaced with dashes.

# Horizontal Rules

Use three asterisks to create a horizontal rule:

`***`

produces

***

# Lists

Ordered lists, begin the line with a period and (any) numeral. Unordered, an asterisk, plus sign, or dash:

~~~~
1. First item
1. Second item
1. Third item
   1. Indent a list item with three leading spaces.
   1. Second indented item
1. List continues...

* Use *, +, - to create a bulletted list item
   * Three leading spaces for a sub-item
      * Or six
         * Or nine
* Back to the margin
~~~~

produces

1. First item
1. Second item
1. Third item
   1. Indent a list item with three leading spaces.
   1. Second indented item
1. List continues...

* Use *, +, - to create a bulletted list item
   * Three leading spaces for a sub-item
      * Or six
         * Or nine
* Back to the margin

# Links

Place the link text in square brackets and the target URL in parentheses: 

`[See this link](https://google.com)` >>>> [See this link](https://google.com)


To link to a heading in the current document, note the auto-generated anchors described above. 

`see the section [Lists](#markdown-header-lists)` >>>> see the section [Lists](#markdown-header-lists)

# Formatting Text Spans

## Emphasis

Markdown treats `*` (asterisk) and `_` (underscore) as emphasis markers. Use one for italics, two for bold, and three for both. 

`*single asterisks*` >>>>  *single asterisks* 
 
`_single underscores_` >>>>  _single underscores_

`**double asterisks**` >>>>  **double asterisks**

`__double underscores__` >>>>  __double underscores__

Combinations work too:

`__double underscores with *embedded asterisks*__` >>>>  __double underscores with *embedded asterisks*__

`_single underscores with **embedded double asterisks**_` >>>>  _single underscores with **embedded double asterisks**_

Use a backslash to escape formatting characters:

`5\*5 = 25` >>>> 5\*5 = 25

## Strikeout

Use two tildes:

`Not ~~yet~~ ever` >>>>  Not ~~yet~~ ever

## Code

To indicate a span of code, wrap it with `` ` `` (accent grave). This allows you to denote a span of code within a normal paragraph. 

``Use the `printf()` function`` >>>> Use the `printf()` function
	
To include an accent grave within a code span, use two accent grave characters to enclose the text.

# Images

To display images inline with text, use a bang, square brackets containing the alt-text to display if the image is not 
available, followed by the URL of the image in parentheses:

`![An Image of Pest](img/pest_from_gellert.jpg)` 

produces:

![An Image of Pest](img/pest_from_gellert.jpg)

You can include a title for the image (which will be displayed as tip text when hovering over the image) 
in quotation marks within the parentheses after the URL:

`![An Image of Pest](img/pest_from_gellert.jpg "Central Pest viewed from Gellért Hill")` 

produces:

![An Image of Pest](img/pest_from_gellert.jpg  "Central Pest viewed from Gellért Hill")

# Tables

You create tables using pipe and dash characters. Tables structure is defined by two rows of text:

* A row with individual header cells separated by pipe characters
* A row with dashes separated by pipe characters

The number of cells must match between the two rows, and this defines the number of columns in the table:

~~~~
Repository | Repo Class | Number of Files
-----------|------------|----------------
~~~~



additional rows contain the table data:

~~~~
Repository | Repo Class | Number of Files
-----------|------------|----------------
atq | A | 23,334
zqr | C | 1,489
lmn | D | 749,221
~~~~
Repository | Repo Class | Number of Files
-----------|------------|----------------
atq | A | 23,334
zqr | C | 1,489
lmn | D | 749,221

The number of dashes in the second row of cells allows you to fix the minimum width of each column (the number of dashes). 
You can also control column alignment using a colon character: At the left end of the cell for left-aligned, 
both ends for centered, and at the right end for right-aligned.

~~~~
Repository | Repo Class | Number of Files
:----------|:----------:|:--------------:
atq | A | 23,334
zqr | C | 1,489
lmn | D | 749,221
~~~~

Repository | Repo Class | Number of Files
:----------|:----------:|---------------:
atq | A | 23,334
zqr | C | 1,489
lmn | D | 749,221


# Code Sections

To include a block of unformatted text, such as a code fragment, place a line with four tilde characters at the 
beginning and end of the block of text:

`~~~~`

_place your code here_

`~~~~`

produces

~~~~
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:2.1.2'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        jcenter()
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
~~~~
